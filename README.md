# Sorry, we have moved

The current Openwind repository is here : https://gitlab.inria.fr/openwind/openwind


# Download old releases

https://gitlab.inria.fr/openwind/release/-/releases/v0.1.0

https://gitlab.inria.fr/openwind/release/-/releases/v0.2.0

https://gitlab.inria.fr/openwind/release/-/releases/v0.3.0
